package com.yubao.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubao.mapper.QuestionMapper;
import com.yubao.request.MsgListReq;
import com.yubao.request.SendMsgReq;
import com.yubao.response.AnswerViewModel;
import com.yubao.response.PageObject;
import com.yubao.mapper.MessageMapper;
import com.yubao.entity.*;

import com.yubao.response.UserViewModel;
import com.yubao.util.NormalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Administrator on 2016-12-20.
 */
@Service
public class MessageService extends ServiceImpl<MessageMapper, Message> implements IService<Message> {

    @Autowired
    MessageMapper _mapper;

    @Autowired
    LoginService _loginService;

    //发送站内私信
    public void send(SendMsgReq req) throws NormalException {
        UserViewModel user = _loginService.get();
        if (user == null)
            throw new NormalException("亲！等个录先~~");

        Message msg = new Message();
        msg.setId(UUID.randomUUID().toString());
        msg.setTo(req.getTo());
        msg.setContent(req.getContent());
        msg.setTime(new Date());
        msg.setType("站内私信");
        msg.setFrom(user.getId());
        msg.setFromname(user.getName());
        msg.setTitle(user.getName() +"发出的新消息");
        save(msg);
    }

    /**
     * 通知问题被别人回复
     *
     * @param user
     * @param question
     */
    public void notify(User user, Question question) {
        if (user.getId().equals(question.getUserid())) {  //自己回复自己的问题
            return;
        }
        Message msg = new Message();
        msg.setId(UUID.randomUUID().toString());
        msg.setTo(question.getUserid());
        msg.setContent(user.getName() + "回复了" + question.getTitle());
        msg.setTime(new Date());
        msg.setType("系统消息");
        msg.setFrom("system");
        msg.setFromname("系统");
        msg.setTitle("通知");
        msg.setHref("/questions/detail?id=" + question.getId());
        save(msg);
    }

    public PageObject<Message> queryMsgList(MsgListReq req) throws Exception {
        UserViewModel user = _loginService.get();
        if (user == null)
            throw new Exception("亲！等个录先~~");

        PageObject<Message> obj = new PageObject<Message>();
        IPage<Message> questionPage = page(new Page<Message>(req.getIndex(), req.getSize()),
                new QueryWrapper<Message>().eq("`to`", user.getId()).or()
                        .eq("`from`",user.getId()).orderByDesc("time"));

        obj.setSize(req.getSize());
        obj.setTotal((int) questionPage.getTotal());
        obj.setObjects(questionPage.getRecords());

        return obj;
    }

    public void del(String id) throws NormalException {
        UserViewModel user = _loginService.get();
        if (user == null)
            throw new NormalException("亲！等个录先~~");
        removeById(id);
    }

    public void delall() throws NormalException {
        UserViewModel user = _loginService.get();
        if (user == null)
            throw new NormalException("亲！等个录先~~");

        _mapper.deleteByUser(user.getId());
    }


}

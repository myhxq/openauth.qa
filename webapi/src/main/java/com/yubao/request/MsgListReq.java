package com.yubao.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class MsgListReq {
    @ApiModelProperty(value = "搜索关键字")

    private String key;
    private int index;
    private int size;

    public MsgListReq() {
        this.key = null;
        this.index = 1;
        this.size = 10;
    }


}

package com.yubao.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class QuestionListReq {
    private String key;
    @ApiModelProperty(value = "resolved：已解决/unresolved：未解决/wonderful：精贴")
    private String type;
    private int index;
    private int size;
}
